const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: './', // 设置打包文件相对路径
  transpileDependencies: true
})
